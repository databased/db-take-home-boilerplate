import React, { Component } from 'react'
import './style.css'

/*
type IProps = {
  points: {
    id: number
    managerId: number
    name: string
    x: number
    y: number
  }[]
}
*/

class Quadrant extends Component {
  mouseEnter = () => {
    document.getElementById('vertical-guide').style.display = 'block'
    document.getElementById('horizontal-guide').style.display = 'block'
  }

  mouseLeave = () => {
    document.getElementById('vertical-guide').style.display = 'none'
    document.getElementById('horizontal-guide').style.display = 'none'
  }

  mouseMove = (event) => {
    const quadrantRect = document.getElementById('graph').getBoundingClientRect()
    const mouseX = event.clientX
    const mouseY = event.clientY
    const quadrantX = mouseX - quadrantRect.left
    const quadrantY = mouseY - quadrantRect.top

    document.getElementById('vertical-guide').style.left = `${quadrantX}px`
    document.getElementById('horizontal-guide').style.top = `${quadrantY}px`
  }

  render () {
    return (
      <div id='quadrant'>
        <div
          id='quadrant-wrapper'
          onMouseEnter={this.mouseEnter}
          onMouseLeave={this.mouseLeave}
          onMouseMove={this.mouseMove}
        >
          <figure id='horizontal-guide'></figure>
          <figure id='vertical-guide'></figure>
          <div id='graph'>
            <figure id='vertical-divider'></figure>
            <figure id='horizontal-divider'></figure>
            <div id='inner-quadrant'>
              {/* this.renderPoints() */}
            </div>
          </div>
        </div>
        <div className='scale-wrapper'>
          <span>0%</span>
          <span>200%</span>
        </div>
      </div>
    )
  }
}

export default Quadrant
